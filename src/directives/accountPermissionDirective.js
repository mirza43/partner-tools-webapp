import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'
import unauthorizedTemplate from './unauthorized.html!text';
import AccountPermissionController from './controller'

function accountPermission($uibModal) {
    return {

        replace: false,
        controller: AccountPermissionController,
        controllerAs: 'controller',
        bindToController: true,
        link : function (scope,element,attributes) {
            scope.loader = true;
            scope.accountPermissionFlag()
                .then((result)=>{
                    var groupId = scope.groupId;
                    if(groupId == 'Employee' || groupId == 'CS' ) {
                        scope.isSpiffAuthorized = false;
                        scope.isEWAuthorized = false;
                        scope.loader = false;
                        scope.isPageLoaded = false;
                        scope.modalInstance = $uibModal.open({
                            scope: scope,
                            template: unauthorizedTemplate,
                            size: 'sm',
                            backdrop: 'static'
                        });

                        scope.closeUnAuthorisedPopup = function(){
                            scope.modalInstance.dismiss('cancel');
                        }
                    }else
                    {
                        if(scope.res.spiff) {
                            element.show();
                            scope.isSpiffAuthorized = true;
                        }else{
                            scope.isSpiffAuthorized = false;
                        }
                        if(scope.res.extendedWarranty) {
                            element.show();
                            scope.isEWAuthorized = true;
                        }else{
                            scope.isEWAuthorized = false;
                        }
                        scope.loader = false;
                        scope.isPageLoaded = true;
                    }
                })
                .catch(error => {
                    var groupId = scope.groupId;
                    if(groupId == 'Employee' || groupId == 'CS' ) {
                        scope.isPageLoaded = false;
                        scope.isEWAuthorized = false;
                        scope.isSpiffAuthorized = false;
                        scope.loader = false;
                        $uibModal.open({
                            scope: scope,
                            template: unauthorizedTemplate,
                            size: 'sm',
                            backdrop: 'static'
                        });
                    }else {
                        scope.isPageLoaded = true;
                        scope.loader = false;
                    }
                });
        }
    }
}

export default accountPermission;